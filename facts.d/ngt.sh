#!/bin/bash

#Test if running on nutanix
VIRT=$(dmidecode -s system-product-name)
if [ "$VIRT" == "AHV" ]
then
  NGT_CONFIG=/usr/local/nutanix/ngt/config/ngt_config.json
  if [ -e $NGT_CONFIG ]; then
    sed -e 's/\"\([^n][^:, ]*\):/"ngt_\1:/g' $NGT_CONFIG
  else
    echo '{"ngt_cluster_ip": "", "ngt_uuid": "", "ngt_version": "0.0.0", "ngt_port": 2074}'
  fi
fi
