# @summary A short summary of the purpose of this class
#
# A description of what this class does
#
# @example
#   include ngt
class ngt (
  String  $src_dev = '/dev/sr0',
  String  $src_mnt = '/tmp/ngtcd',
  Boolean $manage  = false,
  String  $version = '0.0.0',
  String  $python_version = '3',
  Hash    $prereq_pkgs = { 'dmidecode' => {}, 'python3' => {} }
) {
  $python = "python${python_version}"
  $_manage = ( $manage and 'ngt_version' in $facts
       and versioncmp($version,$facts['ngt_version']) > 0)

  $mnt_ensure = $_manage ? {
    true    => 'mounted',
    default => 'absent',
  }
  $dir_ensure = $_manage ? {
    true    => 'directory',
    default => 'absent',
  }
  file { $src_mnt: ensure => $dir_ensure, force => true}
  mount { $src_mnt:
    ensure => $mnt_ensure,
    device => $src_dev,
    fstype => 'iso9660',
    options => 'ro,noauto',
    require => File[$src_mnt],
  }
  unless empty($prereq_pkgs) {
    ensure_resources('package',$prereq_pkgs,
      {
        'ensure' => 'installed',
        'before' => File[$src_mnt],
      })
  }

  if ( $_manage ) {
    exec { "${src_mnt}/installer/linux/install_ngt.py":
      path    => [ '/usr/bin', '/bin', '/usr/sbin', '/sbin'], #needed for scripts
      command => [ $python, "${src_mnt}/installer/linux/install_ngt.py" ],
      onlyif  => "grep ngt_version ${src_mnt}/config/ngt_config.json | grep -q ${version}",
      require => Mount[$src_mnt],
    }
  }
}
